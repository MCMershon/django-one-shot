from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list_list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list_detail = TodoList.objects.get(id=id)
    context = {"todo_list_detail": todo_list_detail}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list_create = form.save()
            return redirect("todo_list_detail", id=todo_list_create.id)
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create-list.html", context)


def todo_list_update(request, id):
    todo_list_update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list_update)
        if form.is_valid():
            todo_list_update = form.save()
            return redirect("todo_list_detail", id=todo_list_update.id)
    else:
        form = TodoListForm(instance=todo_list_update)

    context = {"form": form}

    return render(request, "todos/edit-list.html", context)


def todo_list_delete(request, id):
    todo_list_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item_create = form.save()
            return redirect("todo_list_detail", id=todo_item_create.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/create-item.html", context)


def todo_item_update(request, id):
    todo_item_update = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item_update)
        if form.is_valid():
            todo_item_update = form.save()
            return redirect("todo_list_detail", id=todo_item_update.list.id)
    else:
        form = TodoItemForm(instance=todo_item_update)

    context = {"form": form}

    return render(request, "todos/edit-item.html", context)
